import { configure } from '@kadira/storybook';
import 'todomvc-app-css/index.css'
import '../containers/styles/materialize.min.css'
import '../containers/styles/styles.css'
import 'rc-slider/assets/index.css'
import injectTapEventPlugin from 'react-tap-event-plugin';
import _ from 'lodash';

injectTapEventPlugin();

function loadStories() {
  require('../components/stories/');
}

configure(loadStories, module);
