import { createActionAsync, createReducer, createAction } from '../utils/actions';
import { fromJS } from 'immutable';
import axios from 'axios';
import _ from 'lodash';
import { Lokka } from 'lokka';
import { Transport } from 'lokka-transport-http';
import { map } from 'lodash';
import { persistent, client } from './persistent';

//Get data from server
export const loadIssues = createActionAsync("LOAD_ISSUES", loadIssuesData);
export const loadSingleIssue = createActionAsync("LOAD_SINGLE_ISSUE", loadSingleIssueData);
export const loadPROsOrCONs = createActionAsync("LOAD_PROS", loadPROsOrCONsData);
export const authorise = createActionAsync("SIGN_IN", findUser);
export const registerUser = createActionAsync("REGISTER", SignUp)

export const getConfig = createActionAsync("GET_CONFIG", loadConfig);

//Fetch users answers
export const step1Strength = createAction("STEP1_SET_STRENGTH");
export const changeHeader = createAction("CHANGE_HEADER");
export const step2Strength = createAction("STEP2_SET_STRENGTH");
export const step2Select = createAction("STEP2_SELECT");
export const pickMistake = createAction("PICK_MISTAKE");
export const step3SelectCounterArg = createAction("STEP3_SELECT_COUNTER_ARGUMENT");
export const step4chooseSide = createAction("STEP4_CHOOSE_SIDE");
export const step5Strength = createAction("STEP5_SET_STRENGTH");
export const step6drag = createAction("STEP6_DRAG");
export const step6dragIrrelevant = createAction("STEP6_DRAG_IRRELEVANT");
export const step6CONsDrag = createAction("STEP6_CONS_DRAG");
export const step6CONsDragIrrelevant = createAction("STEP6_CONS_DRAG_IRRELEVANT");

export const finish = createAction("FINISH");
//Load Config
function loadConfig() {
  return client.query(`query{
    configNodes{
      nodes {
        id
        key
        valueText
        valueNumber
        valueJson
      }
    }
  }`).then((res) => {
    const configs = res.configNodes.nodes;
    const getConfig = (key) => {
      const {valueText, valueNumber, valueJson} = _.find(configs, {
        key
      })
      return valueText || valueNumber || valueJson;
    };
    return {
      mistakes: getConfig("mistakes"),
      argumentsAmountToRate: getConfig("argumentsAmountToRate"),
      argumentsAmountToShow: getConfig("argumentsAmountToShow"),
      showFromOneSideOrFromBoth: getConfig("showFromOneSideOrFromBoth"),
      argumentAmountToRank: getConfig("argumentAmountToRank")
    }
  })
}
//Sign in
function findUser({login, pswd}) {
  return client.query(`query($login: String, $pswd: String){
      userNodes(login: $login, password: $pswd) {
        nodes {
          id
        }
      }
    }`, {
    login,
    pswd
  })
}

function SignUp({login, pswd}) {

  return client.query(`query($login: String, $pswd: String){
    userNodes(login: $login, password: $pswd) {
      nodes {
        id
      }
    }
  }`, {
    login,
    pswd
  }).then(({userNodes}) => {
    let usersArray = map(userNodes.nodes, 'id');
    console.log(usersArray);
    if (_.isEmpty(usersArray)) {
      return client.mutate(`($login: String, $pswd: String){insertUser(input:{login: $login, password: $pswd})
          {
            user {
              id
              login
              password
            }
          }
      }
    `, {
        login,
        pswd
      });
    } else {
      return null;
    }
  });
}

//Load issues
function loadIssuesData() {
  return client.query(`query{
      topicNodes(offset:0){
        totalCount
        nodes {
          id
          name
        }
      }
    }`);
}

//Load all issues of chosen Issue--------------------
function loadSingleIssueData({issueId, limit}) {
  return client.query(`query ($issueId:ID!, $limit: Int){
      topic(id: $issueId){
        id
        name
        argumentNodesByTopic(first: $limit) {
          edges {
            node {
              id
              content
              side
              userByAuthorId {
                id
                login
              }
            }
          }
        }
      }
    }`, {
    issueId,
    limit
  })
}

//Load PROs/CONs issues of chosen Issue----------------
function loadPROsOrCONsData(data) {
  console.log(data.id, data.limit);
  return client.query(`query ($id:ID!, $limit: Int){
      topic(id: $id){
        id
        name
        argumentNodesByTopic(first: $limit) {
          edges {
            node {
              id
              content
              side
              userByAuthorId {
                id
                login
              }
            }
          }
        }
      }
    }`, {
    id: data.id,
    limit: data.limit
  })
}

export const PoliticalReducer = createReducer({

  //Load Config data-----------------------------------

  [getConfig.request]: (state) => {
    return state;
  },
  [getConfig.ok]: (state, {mistakes, argumentsAmountToRate, argumentsAmountToShow, showFromOneSideOrFromBoth, argumentAmountToRank}) => {
    console.log(mistakes);
    return state.setIn(['mistake'], mistakes)
      .setIn(['adminArgsToRate'], argumentsAmountToRate)
      .setIn(['argumentsAmountToShow'], argumentsAmountToShow)
      .setIn(['adminOneSide'], showFromOneSideOrFromBoth)
      .setIn(['adminArgsToRank'], argumentAmountToRank)
  },
  [getConfig.error]: (state, error) => {
    return state;
  },

  //Load users data-----------------------------------

  [authorise.request]: (state) => {
    return state;
  },
  [authorise.ok]: (state, {userNodes}) => {
    let usersArray = map(userNodes.nodes, 'id');
    if (_.isEmpty(usersArray)) {
      console.log("USER FOUND?: ", false);
      return state.setIn(['userFound'], false);
    } else {
      console.log("USER FOUND?: ", true);
      return state.setIn(['userFound'], true).setIn(['userId'], _.head(usersArray));
    }
  },
  [authorise.error]: (state, error) => {
    return state;
  },

  //Register new user-----------------------------------

  [registerUser.request]: (state) => {
    return state;
  },
  [registerUser.ok]: (state, data) => {
    if (data === null) {
      console.log("USER EXIST TRUE");
      return state.setIn(['userExists'], true);
    } else {
      console.log(data.insertUser.user.id);
      return state.setIn(['userExists'], false).setIn(['userId'], data.insertUser.user.id);
    }
  },
  [registerUser.error]: (state, error) => {
    return state;
  },

  //Load all data-----------------------------------

  [loadIssues.request]: (state) => {
    return state;
  },
  [loadIssues.ok]: (state, {topicNodes}) => {
    return state.setIn(['issues'], topicNodes.nodes);
  },
  [loadIssues.error]: (state, error) => {
    return state;
  },

  //Load all issues of chosen Issue------------------

  [loadSingleIssue.request]: (state, id) => {
    return state;
  },
  [loadSingleIssue.ok]: (state, {topic}) => {
    return state.setIn(['chosenIssue'], topic).setIn(['args'], map(topic.argumentNodesByTopic.edges, 'node'));
  },
  [loadSingleIssue.error]: (state, error) => {
    return state;
  },

  //Load PROs/CONs issues of chosen Issue-------------

  [loadPROsOrCONs.request]: (state, data) => {
    return state;
  },
  [loadPROsOrCONs.ok]: (state, {topic}) => {
    return state.setIn(['sortedArgs'], map(topic.argumentNodesByTopic.edges, 'node'));
  },
  [loadPROsOrCONs.error]: (state, error) => {
    return state;
  },

  //-------------------------------------------------

  [step1Strength]: (state, issueFetched, strength) => {
    let startChoice = {
      issueId: issueFetched.id,
      strength: strength
    };
    return state.setIn(['initialPosition'], startChoice);
  },
  [changeHeader]: (state, headerFetched) => {
    return state.setIn(['header'], headerFetched);
  },
  [step2Strength]: (state, strength, argFetched) => {
    let newArgs = _.forEach(state.get('args'), (arg) => {
      if (arg.id === argFetched.id) {
        arg.suppStrength = strength;
        arg.mistake = -1;
      }
    });
    return state.setIn(['argumentRatings'], newArgs).set(['args'], newArgs);
  },
  [step2Select]: (state, argFetched, side) => {
    let newArgs = _.forEach(state.get('args'), (arg) => {
      if (arg.id === argFetched.id) {
        arg.side = side;
      }
    });
    return state.setIn(['argumentRatings'], newArgs).set(['args'], newArgs);
  },
  [pickMistake]: (state, argFetched, mistake) => {
    console.log("ARG FETCHED: ", argFetched, "MISTAKE: ", mistake);
    let newArgs = _.forEach(state.get('args'), (arg) => {
      if (arg.id === argFetched.id) {
        arg.mistake = mistake;
      }
    });
    return state.setIn(['argumentRatings'], newArgs).set(['args'], newArgs);
  },
  [step3SelectCounterArg]: (state, fetchedArgPair) => {
    return state.updateIn(['argsPairs'], (oldArray) => oldArray.concat(fetchedArgPair));
  },
  [step4chooseSide]: (state, pairsObj) => {
    return state.updateIn(['step4pairs'], (oldArray) => oldArray.concat(pairsObj));
  },
  [step5Strength]: (state, issueFetched, strength) => {
    let endChoice = {
      issueId: issueFetched.id,
      strength: strength
    };
    if (strength === 2) {
      return state.setIn(['step5res'], endChoice).setIn(['rankCons'], true);
    }
    else return state.setIn(['step5res'], endChoice).setIn(['rankCons'], false);
  },
  [step6drag]: (state, fetchedCards) => {
    let rankedArgs = _.forEach(fetchedCards, (card, i) => {
      card.rank = i + 1;
    });
    return state.setIn(['step6resPros'], rankedArgs)
  },
  [step6dragIrrelevant]: (state, fetchedCards) => {
    let rankedArgs = _.forEach(fetchedCards, (card) => {
      card.rank = 0;
    });
    return state.setIn(['step6resProsIrrelevant'], rankedArgs)
  },

  //---------------------------------------------

  [step6CONsDrag]: (state, fetchedCards) => {
    let rankedArgs = _.forEach(fetchedCards, (card, i) => {
      card.rank = i + 1;
    });
    return state.setIn(['step6resCons'], rankedArgs)
  },
  [step6CONsDragIrrelevant]: (state, fetchedCards) => {
    let rankedArgs = _.forEach(fetchedCards, (card) => {
      card.rank = 0;
    });
    return state.setIn(['step6resConsIrrelevant'], rankedArgs)
  },

  [finish]: persistent
}, fromJS(
  {
    userFound: null,
    userExists: null,
    usedId: '',
    //Admin initial state
    adminArgsToRate: 0,
    adminOneSide: "",
    adminArgsToRank: 0,
    mistake: [],
    argumentsAmountToShow: 0,
    //End
    header: 'Welcome!',
    issues: [],
    sortedArgs: [], //Temp array for loading PROs or CONs issues
    chosenIssue: {}, //Step 0 result
    initialPosition: {}, //Step 1 result
    args: [],
    argumentRatings: [], //Step 2 results
    argsPairs: [], //Step 3 results
    step4pairs: [], //Step 4 result
    step5res: {}, //Step5 result
    rankCons: false, //Goto Step 6 ranks?
    step6resPros: [], //Step6 PROs result
    step6resProsIrrelevant: [], //Step6 PROs result irrelevant
    step6resCons: [], //Step6 CONs result
    step6resConsIrrelevant: [] //Step6 CONs result irrelevant
  }
));
