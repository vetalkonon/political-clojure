import { Lokka } from 'lokka';
import _ from 'lodash';
import { Transport } from 'lokka-transport-http';
import Q from 'q';

export const client = new Lokka({
  transport: new Transport('https://political-clojure.herokuapp.com/graphql/')
});

function saveState(state) {
  let userId = state.get('userId');

  let topicId = state.get('chosenIssue').id;
  let intialUserView = state.get('initialPosition'); // {strength: [0..4]}, initial = true
  let argumentRatings = state.get('argumentRatings'); // [{side:"PRO"/"CON", suppStrength}]
  let argumentPair = state.get('argsPairs').toJS(); // {argumentId: "YXJndW1lbnQ6Mw==", counterArgId: "YXJndW1lbnQ6NA=="}
  let argumentPairingJudgement = state.get('step4pairs').toJS(); // {suppArgId: , opposeArgId: , judgment}
  let secondUserView = state.get('step5res'); // {issueId: "dG9waWM6MQ==", strength: 3}

  let ranksPRO = state.get('step6resPros').concat(state.get('step6resProsIrrelevant')); //[id, rank]
  let ranksCON = state.get('step6resCons').concat(state.get('step6resConsIrrelevant')); //[id, rank]

  //console.log("RESULTS: ", topicId);
  //console.log("initialUserView: ", intialUserView);
  console.log("argumentRatings: ", argumentRatings);
  //console.log("argumentPair:", argumentPair);
  //console.log("argumentPairingJudgement: ", argumentPairingJudgement);
  //console.log("firstUserView: ", firstUserView);
  //console.log("RANKS PRO:", ranksPRO);
  //console.log("RANKS CON:", ranksCON);

  console.log('123')

  const toMutation = (obj) => _.map(obj, (val, key) => key + ":" + JSON.stringify(val)).join(",");

  // Create work session object - all next objects will be linked to it
  client.mutate(`{insertWorkSession(input:{${toMutation({
    userId,
    topicId
  })}}){
    workSession
    {
      id
    }
  }}`).then((res) => {
    const workSession = res.insertWorkSession.workSession.id;

    console.log('after save', workSession);
    // Save initial and second user views
    client.mutate(`{insertUserView(input:{${toMutation({
      workSessionId: workSession,
      initial: true,
      view: intialUserView.strength
    })}}) {clientMutationId}}`);

    client.mutate(`{insertUserView(input:{${toMutation({
      workSessionId: workSession,
      initial: false,
      view: secondUserView.strength
    })}}) {clientMutationId}}`);

    // save argument pairs and their judgments
    argumentPair.forEach(({argumentId, counterArgumentId}) => {
      client.mutate(`{
        insertArgumentPairing(input:{${toMutation({
        firstArg: argumentId,
        secondArg: counterArgumentId,
        relatedness: 5,
        workSessionId: workSession
      })}}) {argumentPairing{id}}}`).then((res) => {
        const pairingId = res.insertArgumentPairing.argumentPairing.id;
        const pairJudgment = _.find(argumentPairingJudgement, {
          opposeArgId: counterArgumentId,
          suppArgId: argumentId
        });
        if (pairJudgment) {
          client.mutate(`{
              insertPairJudgment(input:{${toMutation({
            pairingId: pairingId,
            judgment: pairJudgment.strength
          })}}) {clientMutationId}}`)
        }
      });
    });

    // save argument ratings
    argumentRatings.forEach(({id, side, suppStrength, mistake}) => {
      client.mutate(`{insertArgumentRating(input:{
            direction: ${side.toUpperCase()},
            ${toMutation({
        argument: id,
        rating: suppStrength,
        mistake: mistake
      })}}) {clientMutationId}}`)
    });

    // argument ranking
    ranksPRO.forEach(({id, rank}) => {
      client.mutate(`{insertArgumentRanking(input:{${toMutation({
        argumentId: id,
        priority: rank
      })}}) {clientMutationId}}`)
    });

    if (state.get(['rankCons']) === true) {
      //IF USERS ANSWER ON STEP 5 WAS "NEUTRAL", WE RANK BOTH ARGUMENTS AND COUNTER_ARGUMENTS
      //CONS
      ranksCON.forEach(({id, rank}) => {
        client.mutate(`{insertArgumentRanking(input:{${toMutation({
          argumentId: id,
          priority: rank
        })}}) {clientMutationId}}`)
      })
    }

  });


}

export function persistent(state, fetchedValue) {
  saveState(state);
  return state;
}
