﻿begin;

CREATE EXTENSION pgcrypto;

CREATE TYPE argument_direction AS ENUM ('against', 'support');

CREATE TABLE "user"
(
  id text primary key default gen_random_uuid(),
  login text,
  password text
);

CREATE TABLE topic
(
  id text primary key default gen_random_uuid(),
  name text
);

CREATE TABLE argument
(
  id text primary key default gen_random_uuid(),
  content text,
  created timestamp default now(),
  modified timestamp default now(),
  author_id text REFERENCES "user" (id) ,
  predecessor text REFERENCES argument(id),
  topic text REFERENCES topic(id),
  side integer
);

CREATE TABLE work_session
(
  id text primary key default gen_random_uuid(),
  user_id text REFERENCES "user" (id),
  topic_id text REFERENCES topic(id)
);

-- argument/counterargument and it's judgment
CREATE TABLE argument_pairing
(
  id text primary key default gen_random_uuid(),
  first_arg text REFERENCES argument(id),
  second_arg text REFERENCES argument(id),
  relatedness integer,
  work_session_id text REFERENCES work_session(id),
  confirmed boolean
);

CREATE TABLE pair_judgment
(
  id text primary key default gen_random_uuid(),
  pairing_id text REFERENCES argument_pairing(id),
  judgment integer,
  "time" timestamp
);

-- rating + ranking
CREATE TABLE argument_rating
(
  id text primary key default gen_random_uuid(),
  work_session_id text REFERENCES work_session(id),
  argument text REFERENCES argument(id),
  created timestamp,
  direction argument_direction,
  rating integer,
  mistake integer
);

CREATE TABLE argument_ranking
(
  id text primary key default gen_random_uuid(),
  argument_id text REFERENCES argument(id),
  priority integer
);
-- user view
CREATE TABLE user_view
(
  id text primary key default gen_random_uuid(),
  "time" timestamp,
  initial boolean,
  view integer,
  work_session_id text REFERENCES work_session(id)
);

-- config table

CREATE TABLE config
(
  id text primary key default gen_random_uuid(),
  key text unique,
  description text,
  value_text text,
  value_number integer,
  value_json json,
  -- two columns should be null. That means that only one column of 3 can be not-null
  check ((value_text is null)::int + (value_number is null)::int + (value_json is null)::int = 2)
);

insert into config(key,description,value_text,value_number,value_json) values
  ('mistakes', 'Values for dropdown list, which appears when user chose "Not Valid" option on slider at "RATE individual arguments" step',null,null,
      '[
          {
              "name": "Slippery Slope",
              "description": "If we allow A, that will pave the way for B, which is dramatically worse, therefore we cannot let A to happen..",
              "examples": [
                  {
                      "phrase": "If we allow same-sex marriage, the next thing you know we’ll be allowing polygamy, incest, and bestiality."
                  },
                  {
                      "phrase": "If the government bails out this company, pretty soon everybody is going to be demanding a bailout."
                  }
              ]
          },
          {
              "name": "Correlation not Causation",
              "description": " If B seems to happen when or where A happens, then A must have caused B",
              "examples": [
                  {
                      "phrase": "San Francisco is socially liberal, and also has a lot of startups.  Socially liberal people must be better at innovation."
                  },
                  {
                      "phrase": "The United States has some of the most liberal gun laws in the world; it is also the world’s biggest economy."
                  }
              ]
          },
          {
              "name": "Cherry picking",
              "description": "Using one piece of evidence or data point to support your argument while disregarding other facts or the big picture",
              "examples": [
                  {
                      "phrase": "I tell you those Russians are all violent crooks. Just the other day I was reading about this shootout between Russian gangs."
                  },
                  {
                      "phrase": "Jimmy’s grandfather smoked a pack a day and lived until he was 97, so clearly smoking can’t be THAT bad for you … there’s clearly more to the story!"
                  },
                  {
                      "phrase": "Global warming is definitely a hoax! Just look at all this snow we’re getting in March!"
                  }
              ]
          },
          {
              "name": "Ad hominem",
              "description": "When an argument attacks a person instead of discussing the actual issue",
              "examples": [
                  {
                      "phrase": "No wonder you support Obamacare, you’re a bleeding-hearted liberal."
                  },
                  {
                      "phrase": "Dad: ''''Listen, son. Drugs can damage your brain. Don’t do drugs!''''  Son: ''''Why would I listen to you? I know you did acid and meth back in the 60''s.''''"
                  }
              ]
          },
          {
              "name": "Straw man",
              "description": "Exaggerating or mischaracterizing the opponent’s stance to make it look ridiculous",
              "examples": [
                  {
                      "phrase": "“Reducing military spending is a suicide plan -- it would leave us defenseless against terrorists.”"
                  },
                  {
                      "phrase": "“You can’t go to the movies tomorrow unless you finish your chores” Child: “You never let me do anything fun with my friends! This is illegal!”"
                  }
              ]
          },
          {
              "name": "False dichotomy",
              "description": "Presupposing there are only two choices, when there is really middle ground",
              "examples": [
                  {
                      "phrase": "Take your pick: you can have free enterprise, or you can have Communism."
                  },
                  {
                      "phrase": "You either support the right to have prayer in schools or you’re an atheist!"
                  },
                  {
                      "phrase": "You’re either with us, or you’re against us!"
                  }
              ]
          },
          {
              "name": "Bandwagon / peer pressure",
              "description": "Arguing that something must be correct because many people believe it",
              "examples": [
                  {
                      "phrase": "Skin cream A is the best-selling skin cream in America! Therefore it must be the most effective skin cream available!"
                  },
                  {
                      "phrase": "Nobody has ever brought this up before as a complaint, so I don’t know what you’re getting so upset about!(reverse bandwagon)"
                  }
              ]
          }
    ]'
  ),
  ('argumentsAmountToRate','Determine how many arguments should be rated at "RATE individual arguments" step',null,3,null),
  ('argumentsAmountToShow', 'Determine how many arguments should be showed at "Pair counterarguments" step',null,3,null),
  ('showFromOneSideOrFromBoth', 'Determine should we use "argumentsAmountToShow" value to limit arguments only for one side, or for both sides', 'one', null, null),
  ('argumentAmountToRank', 'Determine how many arguments should be ranked at "RANK arguments" step', null, 10, null);

commit;
