import React, { PropTypes, Component } from 'react';
import IssuesList from '../../components/step0components/issuesList';
import { browserHistory } from 'react-router';

class CreateIssueContainer extends Component {

  constructor(props) {
    super(props);
    props.actions.loadIssues();
  }

  render() {
    return (
      <div className="main-card-container container z-depth-3">
        <IssuesList issues={this.props.issues}
                    actions={this.props.actions}
                    limit={this.props.limit}
        />
      </div>
    );
  }
}

CreateIssueContainer.propTypes = {
  issues: PropTypes.array.isRequired,
  actions: PropTypes.object.isRequired,
  limit: PropTypes.number
};

export default CreateIssueContainer;