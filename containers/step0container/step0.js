import React, { Component, PropTypes } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import Header from '../../components/global_components/header';
import CreateIssueContainer from './createIssueContainer';

import * as Actions from '../../reducers/politicalClojure';

class Step0 extends Component {

    constructor(props) {
        super(props);
        props.actions.changeHeader("Welcome!");
    }

    render() {
        const { actions } = this.props;
        const { header, issues, adminArgsToRate } = this.props.PoliticalReducer;
        return (
            <div className="main">
                <Header headName={header}/>
                <CreateIssueContainer issues={issues}
                                      actions={actions}
                                      limit={adminArgsToRate}
                />
            </div>
        );
    }
}

function mapStateToProps(state) {
    return {
        PoliticalReducer: state.PoliticalReducer.toJS()
    };
}

function mapDispatchToProps(dispatch) {
    return {
        actions: bindActionCreators(Actions, dispatch)
    };
}

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(Step0);
