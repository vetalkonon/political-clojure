import React, { Component, PropTypes } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import Header from '../../components/global_components/header';
import RatePairsContainer from '../step4container/ratePairsContainer';
import * as Actions from '../../reducers/politicalClojure';

class Step4 extends Component {

    render() {
        const { actions } = this.props;
        const { header, args, chosenIssue } = this.props.PoliticalReducer;
        return (
            <div className="main">
                <Header headName={header}/>
                <RatePairsContainer arguments={args}
                                    opposed={args}
                                    actions={actions}
                                    chosenIssue={chosenIssue}
                />
            </div>
        )
    }
}

function mapStateToProps(state) {
    return {
        PoliticalReducer: state.PoliticalReducer.toJS()
    };
}

function mapDispatchToProps(dispatch) {
    return {
        actions: bindActionCreators(Actions, dispatch)
    };
}

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(Step4)
