import React, { PropTypes, Component } from 'react';
import classnames from 'classnames';
import Slider from 'rc-slider';
import { browserHistory } from 'react-router';
import  update from 'react-addons-update';
import _ from 'lodash';

class ratePairsContainer extends Component {

    constructor(props){
        super(props);
        props.actions.loadSingleIssue({issueId: props.chosenIssue.id, limit: null});
        this.state = {
            inactive: true,
            arguments: props.arguments,
            selectedPairs: [], //Pairs of argument <=> counterArguments
            singleOpposed: {},
            singleSupported: _.sample(props.arguments),
            nextStep: false,
            isNeutral: false,
            sliderValue: 0
        };
    }

    componentDidMount() {
        console.log('did mount');
        console.log("length:", this.state.arguments.length);
        let filtered = _.reject(this.props.arguments, (arg)=>{
            return arg.id ===  this.state.singleSupported.id
        });
        this.setState({
            singleOpposed: _.sample(filtered)
        });
    }

    componentDidUpdate() {
        if(this.state.arguments.length < 2) {
            browserHistory.push("/topic/" + this.props.chosenIssue.id + "/decide");
        }
    }

    makeChoice(value) {
        this.setState({inactive: false});
        let reducedArgsArray = _.difference(this.state.arguments, [this.state.singleSupported, this.state.singleOpposed]);
        let singleSupported = _.sample(reducedArgsArray);
        let filtered = _.reject(reducedArgsArray, (arg) => {
            return arg.id ===  singleSupported.id;
        });
        let singleOpposed = _.sample(filtered);
        if( this.state.arguments.length <= 2) {
            let pair = {
                suppArgId: this.state.singleSupported.id,
                opposeArgId: this.state.singleOpposed.id,
                strength: value
            };
            this.setState(update(this.state, {
                selectedPairs: {
                    $push: [pair]
                },
                nextStage: {
                    $set: true
                }
            }));
            this.props.actions.step4chooseSide(pair)
            browserHistory.push("/topic/"+this.props.chosenIssue.id+"/decide")
        }
        else {
            let pair = {
                suppArgId: this.state.singleSupported.id,
                opposeArgId: this.state.singleOpposed.id,
                strength: value
            };
            this.setState(update(this.state, {
                selectedPairs: {
                    $push: [pair]
                },
                arguments: {
                    $set: reducedArgsArray
                },
                singleSupported: {
                    $set: singleSupported
                },
                singleOpposed: {
                    $set: singleOpposed
                }
            }));
            console.log("REDUCED: ", reducedArgsArray);
            this.props.actions.step4chooseSide(pair)
        }

    }

    render() {
        let hidden = classnames({
            "hidden": !this.state.nextStep
        });
        let inactive = classnames({
            "inactive": this.state.inactive === true
        });
        return (
            <div className="main-card-container container z-depth-3">
                <div className="row">
                    <div className="col s6 m6 l6">
                        <span>Argument (supporting):</span>
                        <div className="argument">
                            {this.state.singleSupported.content}
                        </div>
                    </div>
                    <div className="col s6 m6 l6">
                        <span>Argument (opposing):</span>
                        <div className="argument">
                            {this.state.singleOpposed.content}
                        </div>
                    </div>
                </div>
                <div className="row des makeChoiceBox">
                    <span className="or-divider">Drag the slider to indicate <b>which side</b> of this argument pair is better: </span>
                    <div className="row label-row">
                        <div className="col s12 radio-group">
                            <span>Much better</span>
                        </div>
                        <div className="col s12 radio-group">
                            <span>Better</span>
                        </div>
                        <div className="col s12 radio-group">
                            <span>Neutral</span>
                        </div>
                        <div className="col s12 radio-group">
                            <span>Better</span>
                        </div>
                        <div className="col s12 radio-group">
                            <span>Much better</span>
                        </div>
                    </div>
                    <div className="container">
                        <Slider className={`strength-slider  ${inactive}`}
                                tipFormatter={null}
                                dots
                                min={0}
                                max={4}
                                onAfterChange={this.makeChoice.bind(this)}
                        />
                    </div>
                </div>
                <div className={`row padding-right-3 padding-top-2 ${hidden}`}>
                    <button onClick={()=>browserHistory.push("/topic/"+this.props.chosenIssue.id+"/decide")} className="btn light-blue darken-2 white-text right">
                        Next Step
                    </button>
                </div>
            </div>
        )
    }
}

ratePairsContainer.propTypes = {
    actions: PropTypes.object.isRequired,
    chosenIssue: PropTypes.object,
    arguments: PropTypes.array.isRequired,
    opposed: PropTypes.array.isRequired
};

export default ratePairsContainer
