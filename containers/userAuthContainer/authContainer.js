import React, { Component, PropTypes } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import Header from '../../components/global_components/header';
import * as Actions from '../../reducers/politicalClojure';
import SignIn from '../../components/userAuthComponent/signIn';

class AuthContainer extends Component {

    componentWillMount() {
        this.props.actions.getConfig()
    }

    render() {
        const { actions } = this.props;
        const { userFound, userExists } = this.props.PoliticalReducer;
        return (
            <div className="main">
                <Header headName={"Sign In"}/>
                <SignIn actions={actions}
                        userFound={userFound}
                        userExists={userExists}
                />
            </div>
        )
    }
}

function mapStateToProps(state) {
    return {
        PoliticalReducer: state.PoliticalReducer.toJS()
    };
}

function mapDispatchToProps(dispatch) {
    return {
        actions: bindActionCreators(Actions, dispatch)
    };
}

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(AuthContainer)
