import React, { Component, PropTypes } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import Header from '../../components/global_components/header';
import SupportStrengthContainer from './supportStrength';

import * as Actions from '../../reducers/politicalClojure';

class Step1 extends Component {

    render() {
        const { actions } = this.props;
        const { header, chosenIssue } = this.props.PoliticalReducer;
        return (
            <div className="main">
                <Header headName={header}/>
                <SupportStrengthContainer chosenIssue={chosenIssue}
                                          actions={actions}
                />
            </div>
        )
    }
}

function mapStateToProps(state) {
    return {
        PoliticalReducer: state.PoliticalReducer.toJS()
    };
}

function mapDispatchToProps(dispatch) {
    return {
        actions: bindActionCreators(Actions, dispatch)
    };
}

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(Step1);
