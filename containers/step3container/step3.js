import React, { Component, PropTypes } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import Header from '../../components/global_components/header';
import CounterArgs from '../../components/step3components/counterArgs';
import * as Actions from '../../reducers/politicalClojure';

class Step3 extends Component {

    render() {
        const { actions } = this.props;
        const { header, args, chosenIssue, argumentsAmountToShow, adminArgsShow} = this.props.PoliticalReducer;
        return (
            <div className="main">
                <Header headName={header}/>
                <CounterArgs   arguments={args}
                               counterArgs={args}
                               actions={actions}
                               chosenIssue={chosenIssue}
                               iterationLimit={argumentsAmountToShow}
                               limit={adminArgsShow}
                />
            </div>
        )
    }
}

function mapStateToProps(state) {
    return {
        PoliticalReducer: state.PoliticalReducer.toJS()
    };
}

function mapDispatchToProps(dispatch) {
    return {
        actions: bindActionCreators(Actions, dispatch)
    };
}

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(Step3)
