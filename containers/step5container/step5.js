import React, { Component, PropTypes } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import Header from '../../components/global_components/header';
import DecideContainer from './decideContainer';

import * as Actions from '../../reducers/politicalClojure';

class Step5 extends Component {

    render() {
        const { actions } = this.props;
        const { header, chosenIssue, rankCons, adminArgsToRank } = this.props.PoliticalReducer;
        return (
            <div className="main">
                <Header headName={header}/>
                <DecideContainer chosenIssue={chosenIssue}
                                 actions={actions}
                                 rankCons={rankCons}
                                 limit={adminArgsToRank}
                />
            </div>
        )
    }
}

function mapStateToProps(state) {
    return {
        PoliticalReducer: state.PoliticalReducer.toJS()
    };
}

function mapDispatchToProps(dispatch) {
    return {
        actions: bindActionCreators(Actions, dispatch)
    };
}

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(Step5)
