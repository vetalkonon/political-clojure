import React, { Component, PropTypes } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import Header from '../../components/global_components/header';
import AdminContainer from './adminContainer';
import * as Actions from '../../reducers/politicalClojure';

class Admin extends Component {

    render() {
        const { actions } = this.props;
        const { adminArgsToRate, adminArgsShow, adminOneSide, adminArgsToRank} = this.props.PoliticalReducer;
        const defaultValues = {
            adminArgsToRate: adminArgsToRate,
            adminArgsShow: adminArgsShow,
            adminOneSide: adminOneSide,
            adminArgsToRank: adminArgsToRank
        }
        return (
            <div className="main">
                <Header headName={"Admin settings"}/>
                <AdminContainer defaultValues={defaultValues} actions={actions} />
            </div>
        )
    }
}

function mapStateToProps(state) {
    return {
        PoliticalReducer: state.PoliticalReducer.toJS()
    };
}

function mapDispatchToProps(dispatch) {
    return {
        actions: bindActionCreators(Actions, dispatch)
    };
}

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(Admin)
