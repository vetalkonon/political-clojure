import React, { PropTypes, Component } from 'react';
import classnames from 'classnames';
import { browserHistory } from 'react-router';
import _ from 'lodash';

class MainContainer extends Component {

    render() {
        return (
            <div className="main-card-container container z-depth-3 finish-slide">
                <h2 className="center-align">Done! Thanks for participating</h2>
                <div className="row padding-top-2 center-align">
                    <button
                        onClick={() => browserHistory.push("/")}
                        className="btn light-blue darken-2 white-text">
                        Start again!
                    </button>
                </div>
            </div>
        )
    }
}

export default MainContainer
