import React, { PropTypes, Component } from 'react';
import { DragDropContext } from 'react-dnd';
import { ItemTypes } from '../../components/global_components/ItemTypes';
import DropItem from '../../components/step6components/dropItem';
import DropZone from '../../components/step6components/dragZone';
import RemoveZone from '../../components/step6components/removeZone';
import { default as TouchBackend } from 'react-dnd-touch-backend';
import HTML5Backend, { NativeTypes } from 'react-dnd-html5-backend';
import { browserHistory } from 'react-router';
import classnames from 'classnames';
import _ from 'lodash';

class DragAndDrop extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            dropItems: []
        }
    }

    componentWillMount() {
        this.props.actions.loadPROsOrCONs({id:this.props.chosenIssue.id, side:"PRO", limit:this.props.limit});
    }

    componentWillReceiveProps(props) {
        if (props.arguments !== this.props.arguments)
            this.setState({
                dropItems: props.arguments
            });
    }

    removeFromList(card) {
        let array = this.state.dropItems;
        this.setState({
            dropItems: _.reject(array, (item)=>{
                return card.id === item.id
            })
        })
    }

    renderDropItems() {
        const { dropItems } = this.state;
        return _.map(dropItems, ( card , index)=> {
            return (
                <DropItem card={card}
                          key={index}
                          index={index}
                          removeFromList={this.removeFromList.bind(this)}
                />
            )
        })
    }

    finish() {
        browserHistory.push("/thank_you");
        this.props.actions.finish();
    }

    render() {
        let hidden = classnames({
            "hidden": false
        });
        return (
            <div className="main-card-container container z-depth-3 admin-container">
                <div className="row">
                    <div className="col s9 m9 l6">
                        <span>Rank each of the arguments below relating to this proposal:</span>
                        {this.renderDropItems()}
                    </div>
                    <div className="col s3 m3 l6">
                        <div className="blue-grey darken-2 white-text drd-box">
                            <span className="hide-on-med-and-down">Drag good arguments here: (put stronger arguments higher up)</span>
                            <div className="row drd-container">
                                <div className="">
                                    <DropZone rankArgs={this.props.actions.step6drag} />
                                </div>
                            </div>
                        </div>
                        <RemoveZone className="remove-zone"
                                    rankArgsToZero={this.props.actions.step6dragIrrelevant}
                        />
                    </div>
                </div>
                <div className={`row padding-right-3 padding-top-2 ${hidden}`}>

                    <button onClick={this.finish.bind(this)} className="btn light-blue darken-2 white-text right">
                        Finish
                    </button>
                </div>
            </div>
        )
    }
}

DragAndDrop.propTypes = {
    arguments: PropTypes.array.isRequired,
    actions: PropTypes.object.isRequired,
    chosenIssue: PropTypes.object.isRequired,
    limit: PropTypes.number
};

var isMobile = {
    Android: function () {
        return navigator.userAgent.match(/Android/i);
    },
    BlackBerry: function () {
        return navigator.userAgent.match(/BlackBerry/i);
    },
    iOS: function () {
        return navigator.userAgent.match(/iPhone|iPad|iPod/i);
    },
    Opera: function () {
        return navigator.userAgent.match(/Opera Mini/i);
    },
    Windows: function () {
        return navigator.userAgent.match(/IEMobile/i);
    },
    any: function () {
        return (isMobile.Android() || isMobile.BlackBerry() || isMobile.iOS() || isMobile.Opera() || isMobile.Windows());
    }
};
let Source;
if (isMobile.any()) {
    Source = DragDropContext(TouchBackend)(DragAndDrop);
}
else {
    Source = DragDropContext(HTML5Backend)(DragAndDrop);
}
export default Source;
