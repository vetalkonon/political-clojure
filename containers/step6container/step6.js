import React, { Component, PropTypes } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import Header from '../../components/global_components/header';
import DragAndDrop from './DragAndDrop';

import * as Actions from '../../reducers/politicalClojure';

class Step6 extends Component {
    render() {
        const { actions } = this.props;
        const { header, chosenIssue, sortedArgs, adminArgsToRank } = this.props.PoliticalReducer;
        return (
            <div className="main">
                <Header headName={header}/>
                <DragAndDrop arguments={sortedArgs}
                             actions={actions}
                             chosenIssue={chosenIssue}
                             limit={adminArgsToRank}
                />
            </div>
        )
    }
}

function mapStateToProps(state) {
    return {
        PoliticalReducer: state.PoliticalReducer.toJS()
    };
}

function mapDispatchToProps(dispatch) {
    return {
        actions: bindActionCreators(Actions, dispatch)
    };
}

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(Step6)
