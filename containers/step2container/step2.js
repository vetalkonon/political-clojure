import React, { Component, PropTypes } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import Header from '../../components/global_components/header';
import RateContainer from './rateContainer';
import * as Actions from '../../reducers/politicalClojure';

class Step2 extends Component {

    render() {
        const { actions } = this.props;
        const { header, args, chosenIssue, mistake } = this.props.PoliticalReducer;
        return (
            <div className="main">
                <Header headName={header}/>
                <RateContainer arguments={args}
                               actions={actions}
                               chosenIssue={chosenIssue}
                               mistakesList={mistake}
                />
            </div>
        )
    }
}

function mapStateToProps(state) {
    return {
        PoliticalReducer: state.PoliticalReducer.toJS()
    };
}

function mapDispatchToProps(dispatch) {
    return {
        actions: bindActionCreators(Actions, dispatch)
    };
}

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(Step2)
