import React, { PropTypes, Component } from 'react';
import classnames from 'classnames';
import Slider from 'rc-slider';
import SingleArgRate from '../../components/step2components/singleArgRate';
import { browserHistory } from 'react-router';
import _ from 'lodash';

class ChooseArgStrength extends Component {

    constructor(props) {
        super(props);
        this.state = {
            arguments: _.forEach(props.arguments, (arg) => arg.side = 0)

        }
    }

    renderArgument(argument, index) {
        let previous = index > 0 ? this.props.arguments[index - 1] : null;
        let bool;
        if (previous != null) {
            bool = previous.side != "0" && previous.suppStrength != null
        }
        let hidden = classnames({
            "hidden": bool === false
        });
        return (
            <SingleArgRate argument = {argument}
                           key = {argument._id}
                           previous = {previous}
                           hideCSS = {hidden}
                           index = {index}
                           supportOrNot = {this.props.actions.step2Select}
                           choosePos = {this.props.actions.step2Strength}
                           pickMistake = {this.props.actions.pickMistake}
                           mistakesList = {this.props.mistakesList}
            />
        );
    }

    render() {

        let counter = 0;
        _.forEach(this.state.arguments, (argument) => {
            if (argument.side != "0" && argument.suppStrength != null) {
                counter++;
            }
        });
        let hidden = classnames({
            "hidden": counter != this.props.arguments.length
        });
        return (
            <div className="main-card-container container z-depth-3">
                <span>Rate each of the arguments below relating to this proposal:</span>
                <div className="row no-margins">
                    {_.map(this.state.arguments, this.renderArgument.bind(this))}
                </div>
                <div className={`row padding-right-3 padding-top-2 ${hidden}`}>
                    <button
                        onClick={() => browserHistory.push("/topic/" + this.props.chosenIssue.id + "/find_counterarguments")}
                        className="btn light-blue darken-2 white-text right">
                        Next Step
                    </button>
                </div>
            </div>

        )
    }
}

ChooseArgStrength.propTypes = {
    actions: PropTypes.object.isRequired,
    arguments: PropTypes.array.isRequired,
    chosenIssue: PropTypes.object,
    mistakesList: PropTypes.array.isRequired
};

export default ChooseArgStrength
