export { createReducer as createReducer } from 'redux-act';

export function createAction(type) {
  const actionCreator = function(payload, meta) {
    return {
      type: type,
      payload: payload,
      meta: meta
    }
  }

  actionCreator.getType = () => type;
  actionCreator.toString = () => type;

  return actionCreator;
}

export function createActionAsync(description, api, options = {}) {

  let actions = {
    request: createAction(`${description}_REQUEST`),
    ok: createAction(`${description}_OK`),
    error: createAction(`${description}_ERROR`)
  }

  let actionAsync = (...args) => {
    return (dispatch) => {
      dispatch(actions.request());
      return api(...args)
        .then(res => {
          dispatch(actions.ok(res))
        })
        .catch(err => {
          dispatch(actions.error(err))
          if (!options.noRethrow)
            throw err;
        })
    }
  }

  actionAsync.request = actions.request;
  actionAsync.ok = actions.ok;
  actionAsync.error = actions.error;
  return actionAsync;

}
