import React, { PropTypes, Component } from 'react';
import classnames from 'classnames';
import _ from 'lodash';
import { browserHistory} from 'react-router';
import Snackbar from 'material-ui/lib/snackbar';

class SignIn extends Component {

    constructor() {
        super();
        this.text = "TRIGGERED";
        this.state = {
            snackText: "",
            open: false,
            userFound: false,
            userExists: false
        }
    }

    componentWillReceiveProps(props) {
        if (props.userFound === false)
            this.setState({
                snackText: "User not found!",
                open: true
            });
        else if (props.userExists === true)
            this.setState(
                {
                    snackText: "User already exists!",
                    open: true
                }
            );
    }

    componentWillUpdate(props) {
        if (props.userFound === true || props.userExists === false)
                browserHistory.push("/welcome");
    }

    login(e) {
        e.preventDefault();
        if(this.refs.login.value.length < 3) {
            this.setState(
                {
                    snackText: "Login must contain at least 3 characters!",
                    open: true
                }
            );
        }
        else if(this.refs.pswd.value.length  < 4) {
            this.setState(
                {
                    snackText: "Password must contain at least 4 characters!",
                    open: true
                }
            );
        }
        else {
            this.props.actions.authorise({login: this.refs.login.value, pswd: this.refs.pswd.value});
        }
    }

    signUp(e) {
        console.log(this.refs.login.value);
        e.preventDefault();
            if(this.refs.login.value.length < 3) {
                this.setState(
                    {
                        snackText: "Login must contain at least 3 characters!",
                        open: true
                    }
                );
            }
            else if(this.refs.pswd.value.length < 4) {
                this.setState(
                    {
                        snackText: "Password must contain at least 4 characters!",
                        open: true
                    }
                );
            }
            else {
                this.props.actions.registerUser({login: this.refs.login.value, pswd: this.refs.pswd.value});
            }
        }

    handleRequestClose () {
        this.setState({
            open: false
        });
    }

    render() {
        return (
            <div className="main-card-container container z-depth-3  padding-top-2">
                <Snackbar
                    open={this.state.open}
                    message={this.state.snackText}
                    autoHideDuration={4000}
                    onRequestClose={this.handleRequestClose.bind(this)}
                />
                <span className="or-divider">Please, enter your registration data!</span>
                <div className="container">
                    <input placeholder="Enter login" type="text" ref="login" />
                    <input placeholder="Enter password" type="password" ref="pswd" />
                    <div className={`row padding-right-3 padding-top-2 btn-group`}>
                        <button onClick={this.signUp.bind(this)} type="submit" className="btn light-blue darken-2 white-text right">
                            Register!
                        </button>
                        <button onClick={this.login.bind(this)} type="submit" className="btn light-blue darken-4 white-text right">
                            Sign In!
                        </button>
                    </div>
                </div>

            </div>
        )
    }
}

SignIn.propTypes = {
    userFound: PropTypes.bool.isRequired,
    userExists: PropTypes.bool.isRequired,
    actions: PropTypes.object.isRequired
};

export default SignIn
