import React, { PropTypes, Component } from 'react'
import classnames from 'classnames'

class SingleArgument  extends Component {

    selectArg(argument) {
        this.props.selectArgument(this, argument);
    }

    render() {
        let argClass = classnames({
            "isSelected": this.props.selected === this
        });
        return (
            <div className="col s12">
                <div className={`collection pointer ${argClass}`}>
                    <div className={`collection-item inline`} onClick={this.selectArg.bind(this, this.props.argument)}>
                        {this.props.argument.content}
                    </div>
                </div>
            </div>
        )
    }
}
SingleArgument.propTypes = {
    argument: PropTypes.object.isRequired,
    selectArgument: PropTypes.func.isRequired
};

export default SingleArgument
