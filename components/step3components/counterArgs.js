import React, { PropTypes, Component } from 'react';
import classnames from 'classnames';
import SingleArgument from './argumentComponent';
import MdArrowForward from 'react-icons/lib/md/arrow-forward';
import { browserHistory } from 'react-router';
import  update from 'react-addons-update';
import _ from 'lodash';

class CounterArgs  extends Component {

    constructor(props) {
        super(props);
        props.actions.loadSingleIssue({issueId: props.chosenIssue.id, limit: props.limit});
        this.state = {
            selectedArg: null,
            arguments: props.arguments,
            randomArg: _.sample(props.arguments),
            iterations: props.iterationLimit === 0 ? props.arguments.length : props.iterationLimit,
            nextStage: false
        };
    }

    componentDidMount() {
            this.setState({
                arguments: this.props.arguments
            });
    }

    selectArgument(counterArgument) {
        let pair = {
            argumentId: this.state.randomArg.id,
            counterArgumentId: counterArgument.id
        };

        let counter = this.state.iterations - 1;

        // If iterator limit reduced to 1 or arguments count reduced to 1 we save last result and move to next step
        if(this.state.iterations <=1 || this.state.arguments.length <=1) {
            this.setState(update(this.state, {
                nextStage: {
                    $set: true
                }
            }));
            browserHistory.push("/topic/"+this.props.chosenIssue.id+"/rate");
        }
        else {
            //Delete current random argument, because we found a pair for it
            let reducedArgsArray = _.reject(this.state.arguments, (arg)=>{
                return arg.id ===  this.state.randomArg.id
            });
            //_.difference(this.state.arguments, this.state.randomArg);
            this.setState(update(this.state, {
                arguments: {
                    $set: reducedArgsArray
                },
                randomArg: {
                    $set: _.sample(reducedArgsArray)
                },
                iterations: {
                    $set: counter
                }
            }));
        }
        this.props.actions.step3SelectCounterArg(pair);
    }

    renderCounterArguments() {
        return _.map(this.props.counterArgs, (argument) => {
            if(argument.id != this.state.randomArg.id) {
                return (
                    <SingleArgument key={argument.id}
                                    argument={argument}
                                    selectArgument={this.selectArgument.bind(this, argument)}
                                    selected={this.state.selectedCounter}
                    />
                )
            }
            });
    }

    render() {
        return (
            <div className="main-card-container container z-depth-3">
                <div className="row">
                    <div className="col s12 m5 l5">
                        <span className="or-divider">For this argument:</span>
                        <div className="arguments-box">
                            <div className="col s12">
                                <div className={`collection pointer`}>
                                    <div className={`collection-item inline`}>
                                        {this.state.randomArg.content}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="col s12 m1 l1 flex-wrapper">
                        <MdArrowForward />
                    </div>
                    <div className="col s12 m5 l5">
                        <span className="or-divider">Select the best counterargument:</span>
                        {this.renderCounterArguments()}
                    </div>
                </div>
            </div>
        )
    }
}
CounterArgs.propTypes = {
    arguments: PropTypes.array.isRequired,
    counterArgs: PropTypes.array.isRequired,
    actions: PropTypes.object.isRequired,
    chosenIssue: PropTypes.object,
    iterationLimit: PropTypes.number,
    limit: PropTypes.number
};

export default CounterArgs
