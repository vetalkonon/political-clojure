import React, { PropTypes, Component } from 'react';
import classnames from 'classnames';
import Slider from 'rc-slider';
import Popover from 'material-ui/lib/popover/popover';
import RaisedButton from 'material-ui/lib/raised-button';
import Dialog from 'material-ui/lib/dialog';
import RadioButton from 'material-ui/lib/radio-button';
import RadioButtonGroup from 'material-ui/lib/radio-button-group';
import ModalDialog from './modalDialog';
import _ from 'lodash';

const styles = {
    popover: {
        padding: 20
    },
    block: {
        maxWidth: 250
    },
    radioButton: {
        marginBottom: 16
    },
    customContentStyle: {
        width: '75%',
        maxWidth: 'none'
    }
};

class singleArgRate extends Component {

    constructor(props) {
        super(props);
        this.state = {
            inactive: true,
            showBlock: false,
            modalOpen: false
        };
    }

    handleRequestClose (bool, mistake) {
        this.setState({
            modalOpen: bool
        });
        this.props.pickMistake(this.props.argument, mistake);
    }

    support(argument, e) {
        this.props.supportOrNot(argument, e.target.value);
    }

    makeChoice(value, argument) {
        this.setState({
            inactive: false,
            anchorEl: this.refs.sliderComponent.refs.slider,
            showBlock: true,
            modalOpen: argument === 0
        });
        this.props.choosePos(argument, value);
    }

    render() {
        let inactive = classnames({
            "inactive": this.state.inactive === true
        });
        return (
            <div className={`row des makeChoiceBox ${this.props.hideCSS}`}>
                <div className="col s12 m6 l6 argument-container">
                    <div className="collection argument-box">
                        <div className="collection-item">{this.props.argument.content}</div>
                    </div>
                    <div className="input-field supp-op-box">
                        <select defaultValue="0" onChange={this.support.bind(this, this.props.argument)}>
                            <option value="0" disabled>Select:</option>
                            <option value="support">Supporting</option>
                            <option value="against">Against</option>
                        </select>
                    </div>
                </div>
                <div className="col s12 m6 l6">
                    <div className="row label-row">
                        <div className="col s12 radio-group">
                            <span>Not valid</span>
                        </div>
                        <div className="col s12 radio-group">
                            <span>Irrelevant</span>
                        </div>
                        <div className="col s12 radio-group">
                            <span>Weak</span>
                        </div>
                        <div className="col s12 radio-group">
                            <span>OK</span>
                        </div>
                        <div className="col s12 radio-group">
                            <span>Strong</span>
                        </div>
                    </div>
                    <div className="container" ref="container">
                        <Slider onAfterChange={this.makeChoice.bind(this, this.props.argument)}
                                className={`strength-slider ${inactive}`}
                                tipFormatter={null}
                                dots
                                min={0}
                                max={4}
                                ref="sliderComponent"
                        />
                        <Dialog title={`Why does ${this.props.argument.content} is invalid?`}
                                modal={true}
                                open={this.state.modalOpen}
                                contentStyle={styles.customContentStyle}
                        >
                           <ModalDialog mistakes={this.props.mistakesList}
                                        closeDialog={this.handleRequestClose.bind(this)} />
                        </Dialog>
                    </div>
                </div>
            </div>
        )
    }
}

singleArgRate.propTypes = {
    argument: PropTypes.object.isRequired,
    supportOrNot: PropTypes.func.isRequired,
    choosePos: PropTypes.func.isRequired,
    pickMistake: PropTypes.func.isRequired,
    previous: PropTypes.object,
    hideCSS: PropTypes.string,
    mistakesList: PropTypes.array.isRequired
};

export default singleArgRate