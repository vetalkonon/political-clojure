import React, { PropTypes, Component } from 'react';
import classnames from 'classnames';
import Slider from 'rc-slider';

class ChoosePosition extends Component {

    constructor() {
        super();
        this.state = {
            inactive: true
        }
    }

    makeChoice(value) {
        this.setState({
            inactive: false
        });
        this.props.choosePos(this.props.issue, value);
        this.props.nextStep();
    }

    render() {
        let inactive = classnames({
            "inactive": this.state.inactive === true
        });
        return (
            <div className="row des makeChoiceBox">
                <div className="row label-row">
                    <div className="col s12 radio-group">
                        <span>Strongly oppose</span>
                    </div>
                    <div className="col s12 radio-group">
                        <span>Oppose</span>
                    </div>
                    <div className="col s12 radio-group">
                        <span>Neutral</span>
                    </div>
                    <div className="col s12 radio-group">
                        <span>Support</span>
                    </div>
                    <div className="col s12 radio-group">
                        <span>Strongly support</span>
                    </div>
                </div>
                <div className="container">
                    <Slider onAfterChange={this.makeChoice.bind(this)}
                            className={`strength-slider last-hidden ${inactive}`}
                            tipFormatter={null}
                            dots
                            min={0}
                            max={4}
                    />
                </div>
            </div>
        )
    }
}

ChoosePosition.propTypes = {
    choosePos: PropTypes.func.isRequired,
    issue: PropTypes.object.isRequired,
    nextStep: PropTypes.func.isRequired
};

export default ChoosePosition