export const Issues = [
    {
        id: 1,
        name: "issue 1"
    },
    {
        id: 2,
        name: "issue 2"
    },
    {
        id: 3,
        name: "issue 3"
    },
    {
        id: 4,
        name: "issue 4"
    },
    {
        id: 5,
        name: "issue 5"
    },
    {
        id: 6,
        name: "issue 6"
    },
    {
        id: 7,
        name: "issue 7"
    }
];
export const OneArg = [
    {
        id: 0,
        topic: "Developed countries should accept more refugees from crisis regions (e.g, Syrian war)",
        text: "First Argument Here",
        notValid: false,
        rank: 0,
        isSupported: true,
        supStrength: 0,
        counterArguments: []
    }
];
export const TwoArgs = [
    {
        id: 0,
        topic: "Developed countries should accept more refugees from crisis regions (e.g, Syrian war)",
        text: "First Argument Here",
        notValid: false,
        rank: 0,
        isSupported: true,
        supStrength: 2,
        counterArguments: []
    },
    {
        id: 1,
        topic: "Developed countries should accept more refugees from crisis regions (e.g, Syrian war)",
        text: "Second Argument Here",
        notValid: false,
        rank: 0,
        isSupported: true,
        supStrength: 0,
        counterArguments: []
    }
];
export const SeveralArgs = [
    {
        id: 0,
        text: "First argument here",
        notValid: false
    },
    {
        id: 1,
        text: "Second argument here",
        notValid: true
    }
];
export const ManyArgs = [
    {
        id: 0,
        topic: "Developed countries should accept more refugees from crisis regions (e.g, Syrian war)",
        text: "First argument here",
        notValid: false,
        rank: 0,
        isSupported: true,
        supStrength: 3,
        counterArguments: []
    },
    {
        id: 1,
        topic: "Developed countries should accept more refugees from crisis regions (e.g, Syrian war)",
        text: "Second argument here",
        notValid: true,
        rank: 0,
        isSupported: true,
        supStrength: 2,
        counterArguments: []
    },
    {
        id: 2,
        topic: "Developed countries should accept more refugees from crisis regions (e.g, Syrian war)",
        text: "Third argument here",
        notValid: true,
        rank: 0,
        isSupported: true,
        supStrength: 3,
        counterArguments: []
    },
    {
        id: 3,
        topic: "Developed countries should accept more refugees from crisis regions (e.g, Syrian war)",
        text: "Fourth argument here",
        notValid: true,
        rank: 0,
        isSupported: true,
        supStrength: 0,
        counterArguments: []
    }
];
export const ManyArgsOppose = [
    {
        id: 0,
        topic: "Developed countries should accept more refugees from crisis regions (e.g, Syrian war)",
        text: "First oppose argument here",
        notValid: false,
        rank: 0,
        isSupported: true,
        supStrength: 0,
        counterArguments: []
    },
    {
        id: 1,
        topic: "Developed countries should accept more refugees from crisis regions (e.g, Syrian war)",
        text: "Second oppose argument here",
        notValid: true,
        rank: 0,
        isSupported: true,
        supStrength: 3,
        counterArguments: []
    },
    {
        id: 2,
        topic: "Developed countries should accept more refugees from crisis regions (e.g, Syrian war)",
        text: "Third oppose argument here",
        notValid: true,
        rank: 0,
        isSupported: true,
        supStrength: 3,
        counterArguments: []
    },
    {
        id: 3,
        topic: "Developed countries should accept more refugees from crisis regions (e.g, Syrian war)",
        text: "Fourth oppose argument here",
        notValid: true,
        rank: 0,
        isSupported: true,
        supStrength: 3,
        counterArguments: []
    }
];