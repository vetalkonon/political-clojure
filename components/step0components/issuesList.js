import React, { PropTypes, Component } from 'react';
import classnames from 'classnames';
import _ from 'lodash';
import { browserHistory, Link } from 'react-router';

class IssuesList extends Component {

  selectIssue(issue) {
    this.props.actions.loadSingleIssue({issueId: issue.id, limit: this.props.limit});
    this.props.actions.changeHeader("Topic: " + issue.name);
    browserHistory.push("topic/" + issue.id + "/rate_topic");
  }

  pickRandomIssue() {
    let issue = _.sample(this.props.issues);
    this.props.actions.loadSingleIssue({issueId: issue.id, limit: this.props.limit});
    this.props.actions.changeHeader("Topic: " + issue.name);
    browserHistory.push("topic/" + issue.id + "/rate_topic");
  }

  renderList() {
    return this.props.issues.map((issue) => (
      <li onClick={this.selectIssue.bind(this, issue)} key={issue.id} className="collection-item pointer">
        <a>{issue.name}</a>
      </li>
    )
    );
  }

  render() {
    return (
      <div>
      <span>Select an issue: </span>
      <ul className="collection margin-top-3">
        {this.renderList()}
      </ul>
      <a onClick={this.pickRandomIssue.bind(this)} className="btn light-blue darken-2 white-text">Pick a random one</a>
    </div>
    )
  }
}

IssuesList.propTypes = {
  issues: PropTypes.array.isRequired,
  limit: PropTypes.number
};

export default IssuesList
