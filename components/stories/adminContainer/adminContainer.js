import React, { PropTypes, Component } from 'react';
import classnames from 'classnames';
import RadioButton from 'material-ui/lib/radio-button';
import RadioButtonGroup from 'material-ui/lib/radio-button-group';
import _ from 'lodash';

class adminContainer extends Component {

    render() {
        const { defaultValues } = this.props;
        return (
            <div className="main-card-container container z-depth-3 admin-container">
                <div className="row">
                    <div className="col s6 m5 l5">
                        <span>Step 2: Number of arguments to rate </span>
                    </div>
                    <div className="col s6 m6 l6">
                        <input defaultValue={defaultValues.ArgsNumToRate} type="number"/>
                    </div>
                </div>
                <div className="row">
                    <div className="col s6 m5 l5">
                        <span>Step 3: Number of arguments to show </span>
                    </div>
                    <div className="col s6 m6 l6">
                        <input defaultValue={defaultValues.ArgsNumToShow} type="number"/>
                        <RadioButtonGroup labelPosition="left" className="row" name="sides" defaultSelected={defaultValues.OneOrBothSides}>
                            <RadioButton
                                className="col s6 m6 l6 half"
                                value={"one"}
                                label="From one side"
                            />
                            <RadioButton
                                className="col s6 m6 l6 half"
                                value={"both"}
                                label="From both sides"
                            />
                        </RadioButtonGroup>
                    </div>
                </div>
                <div className="row">
                    <div className="col s6 m5 l5">
                        <span>Step 6: Number of arguments to rank </span>
                    </div>
                    <div className="col s6 m6 l6">
                        <input defaultValue={defaultValues.ArgsNumToRank} type="number"/>
                    </div>
                </div>
            </div>
        )
    }
}

adminContainer.propTypes = {
    actions: PropTypes.object.isRequired,
    defaultValues: PropTypes.object.isRequired
};

export default adminContainer
