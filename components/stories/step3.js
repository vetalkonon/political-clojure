import React from 'react';
import Header from './global_components/header';
import { storiesOf, action } from '@kadira/storybook';
import { Issues, OneArg, SeveralArgs, ManyArgs } from './global_components/issues_export';
import CounterArgs from './step3components/counterArgs';

storiesOf('Step 3', module)
    .add('Fund counterarguments', () => {
        const state = {
            actions: {
                selectArgument: action("Select Argument"),
                selectCounterArg: action("Select CounterArgument")
            },
            arguments: ManyArgs,
            header: "Topic: Developed countries should accept more refugees from crisis regions (e.g, Syrian war)"
        };
        return (
            <div className="main">
                <Header headName={state.header}/>
                <CounterArgs arguments={state.arguments} actions={state.actions}/>
            </div>
        )});