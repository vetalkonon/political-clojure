import React from 'react';
import Header from './global_components/header';
import { storiesOf, action } from '@kadira/storybook';
import AdminContainer from './adminContainer/adminContainer';

storiesOf('Admin settings', module)
    .add('Settings for user exercise', () => {
        const state = {
            header: "Topic: Developed countries should accept more refugees from crisis regions (e.g, Syrian war)",
            defaultValues: {
                ArgsNumToRate: 10,
                ArgsNumToShow: 5,
                OneOrBothSides: "one",
                ArgsNumToRank: 10
            },
            actions: {
                setArgsNumToRate: action("Number of arguments to rate"),
                setArgsNumToShow: action("Number of arguments to show"),
                setOneOrBothSides: action("From one side or From both sides"),
                setArgsNumToRank: action("Step 6: Number of arguments to rank")
            }
        };
        return (
            <div className="main">
                <Header headName={state.header}/>
                <AdminContainer defaultValues={state.defaultValues} actions={state.actions} />
            </div>
        )});