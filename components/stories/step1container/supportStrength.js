import React, { PropTypes, Component } from 'react';
import classnames from 'classnames';
import ChoosePosition from '../step1components/choosePosition';

class supportStrengthContainer extends Component {

    render() {
        return (
            <div className="main-card-container container z-depth-3">
                <span className="or-divider">Before thinking about it too much, would you say you support or oppose this proposition?</span>
                <ChoosePosition choosePos={this.props.actions.choosePos}/>
            </div>
        )
    }
}

supportStrengthContainer.propTypes = {
    actions: PropTypes.object.isRequired
};

export default supportStrengthContainer
