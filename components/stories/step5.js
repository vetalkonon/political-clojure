import React from 'react';
import Header from './global_components/header';
import { storiesOf, action } from '@kadira/storybook';
import { ManyArgs } from './global_components/issues_export';
import DecideContainer from './step5container/decideContainer';
storiesOf('Step 5', module)
    .add('Decide your position', () => {
        const state = {
            header: "Topic: Developed countries should accept more refugees from crisis regions (e.g, Syrian war)",
            actions: {
                choosePos: action("Choose Position")
            }
        };
        return (
            <div className="main">
                <Header headName={state.header}/>
                <DecideContainer actions={state.actions}/>
            </div>
        )});