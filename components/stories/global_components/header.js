import React, { PropTypes, Component } from 'react'

class Header extends Component {

    render() {
        return (
            <header className="main-header z-depth-1 light-blue darken-2 white-text">
                <span>{this.props.headName}</span>
            </header>
        )
    }
}

Header.propTypes = {
    headName: PropTypes.string.isRequired
};

export default Header
