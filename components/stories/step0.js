import React from 'react';
import Header from './global_components/header';
import { storiesOf, action } from '@kadira/storybook';
import { Issues, emptyArg, filledArgs} from './global_components/issues_export';
import CreateIssueContainer from './step0containers/createIssueContainer';

storiesOf('Step 0', module)
    .add('Choose an issue', () => {
        const state = {
            issues: Issues,
            actions: {
                selectIssue: action("Select Issue"),
                pickRandom: action("Pick random issue")
            },
            header: 'Welcome'
        };
        return (
            <div className="main">
                <Header headName={state.header}/>
                <CreateIssueContainer issues={state.issues}
                                      actions={state.actions}
                />
            </div>
        )
    });