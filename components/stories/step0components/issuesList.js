import React, { PropTypes, Component } from 'react';
import classnames from 'classnames';
import _ from 'lodash';

class IssuesList extends Component {

    selectIssue(issue) {
        console.log(issue)
        this.props.selectIssue(issue);
    }

    pickRandomIssue() {
        console.log("Loaded More");
        this.props.pickRandom( _.sample(this.props.issues));
    }

    renderList() {
        return this.props.issues.map((issue) =>
            (
                <li onClick={this.selectIssue.bind(this, issue)}
                    key={issue.id}
                    className="collection-item pointer">
                    <a>{issue.name}</a>
                </li>
            )
        );
    }

    render() {
        return (
            <div>
                <span>Select an issue: </span>
                <ul className="collection margin-top-3">
                    {this.renderList()}
                </ul>
                <a onClick={this.pickRandomIssue.bind(this)} className="btn light-blue darken-2 white-text">Pick a random one</a>
            </div>
        )
    }
}

IssuesList.propTypes = {
    issues: PropTypes.array.isRequired,
    selectIssue: PropTypes.func.isRequired,
    pickRandom: PropTypes.func.isRequired
};

export default IssuesList
