import React, { PropTypes, Component } from 'react'
import classnames from 'classnames'

class SingleArgument  extends Component {

    selectArg(id, e) {
        this.props.selectArgument.selectArgument(id);
        this.props.selectArgument.changeCSS(this);
    }

    render() {
        let argClass = classnames({
            "isSelected": this.props.selected === this
        });
        return (
            <div className="col s12">
                <div className={`collection pointer ${argClass}`}>
                    <div className={`collection-item inline`} onClick={this.selectArg.bind(this, this.props.argument.id)}>
                        {this.props.argument.text}
                    </div>
                </div>
            </div>
        )
    }
}
SingleArgument.propTypes = {
    argument: PropTypes.object.isRequired,
    selectArgument: PropTypes.object.isRequired
};

export default SingleArgument
