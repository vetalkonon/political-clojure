import React, { PropTypes, Component } from 'react';
import cx from 'classnames';
import SingleArgument from './argumentComponent';
import MdArrowForward from 'react-icons/lib/md/arrow-forward';
import _ from 'lodash';

class CounterArgs  extends Component {

    constructor() {
        super();
        this.state = {
            selectedArg: null,
            selectedCounter: null
        };
    }

    selectCSS(argType, obj) {
        if( argType === 0) {
            this.setState({
                selectedArg: obj
            });
        }
        else this.setState({
            selectedCounter: obj
        });
    }

    renderArguments() {
        return this.props.arguments.map((argument) =>  (
            <SingleArgument key={argument.id}
                            argument={argument}
                            selectArgument={{selectArgument: this.props.actions.selectArgument, changeCSS:  this.selectCSS.bind(this, 0)}}
                            selected={this.state.selectedArg}
            />
        ));
    }

    renderCounterArguments() {
        return this.props.arguments.map((argument) =>  (
            <SingleArgument key={argument.id}
                            argument={argument}
                            selectArgument={{selectArgument: this.props.actions.selectArgument, changeCSS:  this.selectCSS.bind(this, 1)}}
                            selected={this.state.selectedCounter}
            />
        ));
    }

    render() {
        return (
            <div className="main-card-container container z-depth-3">
                <div className="row">
                    <div className="col s12 m5 l5">
                        <span className="or-divider">For this argument:</span>
                        <div className="arguments-box">
                            <SingleArgument key={this.props.arguments[0].id}
                                            argument={this.props.arguments[0]}
                                            selectArgument={{selectArgument: this.props.actions.selectArgument, changeCSS:  this.selectCSS.bind(this, 1)}}
                                            selected={this.state.selectedCounter}
                            />
                        </div>
                    </div>
                    <div className="col s12 m1 l1 flex-wrapper">
                        <MdArrowForward />
                    </div>
                    <div className="col s12 m5 l5">
                        <span className="or-divider">Select the best counterargument:</span>
                        {this.renderCounterArguments()}
                    </div>
                </div>
            </div>
        )
    }
}
CounterArgs.propTypes = {
    arguments: PropTypes.array.isRequired,
    actions: PropTypes.object.isRequired
};

export default CounterArgs
