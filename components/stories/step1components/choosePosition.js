import React, { PropTypes, Component } from 'react';
import classnames from 'classnames';
import Slider from 'rc-slider';

class ChoosePosition extends Component {

    makeChoice(value) {
        this.props.choosePos(value)
    }

    render() {
        return (
            <div className="row des makeChoiceBox">
                <div className="row label-row">
                    <div className="col s12 radio-group">
                        <span>Strongly oppose</span>
                    </div>
                    <div className="col s12 radio-group">
                        <span>Oppose</span>
                    </div>
                    <div className="col s12 radio-group">
                        <span>Neutral</span>
                    </div>
                    <div className="col s12 radio-group">
                        <span>Support</span>
                    </div>
                    <div className="col s12 radio-group">
                        <span>Strongly support</span>
                    </div>
                </div>
                <div className="container">
                    <Slider onChange={this.makeChoice.bind(this)}
                            className="strength-slider"
                            tipFormatter={null}
                            dots
                            min={0}
                            max={4}
                            defaultValue={0}  />
                </div>
            </div>
        )
    }
}

ChoosePosition.propTypes = {
    choosePos: PropTypes.func.isRequired
};

export default ChoosePosition