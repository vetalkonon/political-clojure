import React, { PropTypes, Component } from 'react';
import { DragSource } from 'react-dnd';
import { ItemTypes } from '../global_components/ItemTypes'

const boxSource = {
    beginDrag(props) {
        return props.item;
    },

    endDrag(props, monitor) {
        const item = monitor.getItem();
        const dropResult = monitor.getDropResult();
        item.dragged = true;
        if (dropResult) {
            console.log("WE DROPPED: ", item);
        }
    }
};

function collect(connect, monitor) {
    return {
        connectDragSource: connect.dragSource(),
        isDragging: monitor.isDragging()
    }
}

class DropItem extends React.Component {

    render() {
        const { item, connectDragSource } = this.props;

        return connectDragSource(
            <div className="collection">
                <div className="collection-item inline">{item.text}</div>
            </div>
        )}

}

DropItem.propTypes = {
    connectDragSource: PropTypes.func.isRequired,
    isDragging: PropTypes.bool.isRequired,
    item: PropTypes.object.isRequired
};

export default DragSource(ItemTypes.ARGUMENT, boxSource, collect)(DropItem);
