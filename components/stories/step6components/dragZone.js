import React, { PropTypes, Component } from 'react';
import  update from 'react-addons-update';
import { DropTarget } from 'react-dnd';
import { ItemTypes } from '../global_components/ItemTypes';
import  Card  from './sortableDragCards';
import _ from 'lodash';

const dustbinTarget = {
    drop(props ,monitor, DropZone) {
        const item = monitor.getItem();
        if(_.isEmpty(DropZone.state.cards)) {
            DropZone.state.cards.push(item);
        }
        else if(!_.includes(DropZone.state.cards, item)){
            DropZone.state.cards.push(item);
        }
    }
};

function collect(connect, monitor) {
    return {
        connectDropTarget: connect.dropTarget(),
        isOver: monitor.isOver(),
        canDrop: monitor.canDrop()
    }
}

class DropZone extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            cards: []
        };
        this.moveCard = this.moveCard.bind(this);
    }

    moveCard(dragIndex, hoverIndex) {
        const { cards } = this.state;
        const dragCard = cards[dragIndex]; //IS NEEDED FOR REDUCER
        this.setState(update(this.state, {
            cards: {
                $splice: [
                    [dragIndex, 1],
                    [hoverIndex, 0, dragCard]
                ]
            }
        }));
    }
    renderRankedItems(){
        return _.map(this.state.cards, (card, i) =>{
            return (
                <Card key={card.id}
                      index={i}
                      card={card}
                      moveCard={this.moveCard} />
            )
        })
    }

    render() {
        const { isOver, canDrop, connectDropTarget } = this.props;
        const isActive = isOver && canDrop;

        let backgroundColor = '#8bc34a';
        if (isActive) {
            backgroundColor = '#689f38';
        } else if (canDrop) {
            backgroundColor = 'darkkhaki';
        }

        return connectDropTarget(
            <div style={{backgroundColor }} className="col s12 m12 l12 dropZone">
                <ul className="collection">
                    {this.renderRankedItems()}
                </ul>
            </div>
        );

    }
}

DropZone.propTypes = {
    connectDropTarget: PropTypes.func.isRequired,
    isOver: PropTypes.bool.isRequired,
    canDrop: PropTypes.bool.isRequired,
    //onDrop: PropTypes.function.isRequired
};

export default DropTarget(ItemTypes.ARGUMENT, dustbinTarget, collect)(DropZone);
