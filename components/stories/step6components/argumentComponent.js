import React, { PropTypes, Component } from 'react';
import classnames from 'classnames';
import MdCheck from 'react-icons/lib/md/check';

class argumentComponent extends Component {

    constructor() {
        super();
        this.state = {
            checked: false
        }
    }

    check () {
        let checked = !this.state.checked;
        this.setState({
            checked: checked
        });
        console.log(this.state.checked)
    }

    render() {
        let checkedClass = classnames({
            "hidden": this.state.checked === false
        });
        return (
            <li className="col s12 m6 l6 relative" onClick={this.check.bind(this)}>
                <div className="light-blue darken-2 white-text">{this.props.argumentData.text} <MdCheck className={`check-ico ${checkedClass}`} /></div>
            </li>
        )
    }
}

argumentComponent.propTypes = {
    argumentData: PropTypes.object.isRequired,
    checkArgument: PropTypes.func.isRequired
};

export default argumentComponent
