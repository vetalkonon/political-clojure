import React, { PropTypes, Component } from 'react';
import { DropTarget } from 'react-dnd';
import { ItemTypes } from '../global_components/ItemTypes';
import _ from 'lodash';
import FaTrash from '../../../node_modules/react-icons/lib/fa/trash';

const dustbinTarget = {
    drop(props, monitor) {
        props.onDrop(monitor.getItem());
    }
};

function collect(connect, monitor) {
    return {
        connectDropTarget: connect.dropTarget(),
        isOver: monitor.isOver(),
        canDrop: monitor.canDrop()
    }
}

class removeZone extends React.Component {

    render() {
        const { isOver, canDrop, connectDropTarget, lastDroppedItem } = this.props;
        const isActive = isOver && canDrop;

        let backgroundColor = '#d32f2f';
        if (isActive) {
            backgroundColor = '#f44336';
        } else if (canDrop) {
            backgroundColor = '#b71c1c';
        }

        return connectDropTarget(
            <div className="white-text delete-box" style={{backgroundColor }}>
                <span>Drag bad or irrelevant arguments here: <FaTrash className="margin-left-3" /></span>
            </div>
        );

    }
}

removeZone.propTypes = {
    connectDropTarget: PropTypes.func.isRequired,
    isOver: PropTypes.bool.isRequired,
    canDrop: PropTypes.bool.isRequired,
    onDrop: PropTypes.func.isRequired
};

export default DropTarget(ItemTypes.ARGUMENT, dustbinTarget, collect)(removeZone);
