import React from 'react';
import Header from './global_components/header';
import { storiesOf, action } from '@kadira/storybook';
import { ManyArgs, ManyArgsOppose } from './global_components/issues_export';
import RatePairsContainer from './step4container/ratePairsContainer';

storiesOf('Step 4', module)
    .add('JUDGE argument pairs', () => {
        const state = {
            header: "Topic: Developed countries should accept more refugees from crisis regions (e.g, Syrian war)",
            actions: {
                choosePos: action("Choose Position")
            },
            supportArgs: ManyArgs,
            opposeArgs: ManyArgsOppose
        };
        return (
            <div className="main">
                <Header headName={state.header}/>
                <RatePairsContainer actions={state.actions} supported={state.supportArgs} opposed={state.opposeArgs} />
            </div>
        )});