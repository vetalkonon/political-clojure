import React from 'react';
import Header from './global_components/header';
import { storiesOf, action } from '@kadira/storybook';
import { Issues, OneArg, SeveralArgs, ManyArgs, TwoArgs } from './global_components/issues_export';
import DragTest from './step6container/dragTest';

storiesOf('Step 6', module)
    .add('Rank Arguments', () => {
        const state = {
            header: "Topic: Developed countries should accept more refugees from crisis regions (e.g, Syrian war)",
            arguments: ManyArgs,
            actions: {
                setArgsNumToRank: action("Step 6: Number of arguments to rank")
            }
        };
        return (
            <div className="main">
                <Header headName={state.header}/>
                <DragTest arguments={state.arguments} />
            </div>
        )});