import React, { PropTypes, Component } from 'react';
import { DragDropContext } from 'react-dnd';
import { ItemTypes } from '../global_components/ItemTypes';
import DropItem from '../step6components/dragable';
import DropZone from '../step6components/dragZone';
import RemoveZone from '../step6components/removeZone';
import { default as TouchBackend } from 'react-dnd-touch-backend';
import HTML5Backend, { NativeTypes } from 'react-dnd-html5-backend';
import _ from 'lodash';

class dragTest extends React.Component {

    constructor(props) {
        super(props);
            this.state= {
                dropItems: props.arguments
            }
        };

    renderDropItems() {
        const { dropItems } = this.state;
        return _.map(dropItems, ( item , index)=> {
            return (
                <DropItem item={item}
                          key={index}/>
            )
        })
    }

    render() {
        return (
            <div className="main-card-container container z-depth-3 admin-container">
                <div className="row">
                    <div className="col s12 m6 l6">
                        <span>Rank each of the arguments below relating to this proposal:</span>
                        {this.renderDropItems()}
                    </div>
                    <div className="col s12 m6 l6">
                        <div className="blue-grey darken-2 white-text drd-box">
                            <span>Drag good arguments here: (put stronger arguments higher up)</span>
                            <div className="row drd-container">
                                <div className="">
                                    <DropZone />
                                </div>
                            </div>
                        </div>
                        <RemoveZone className="remove-zone" />
                    </div>
                </div>
            </div>
        )
    }
}

dragTest.propTypes = {
    arguments: PropTypes.array.isRequired
};

var isMobile = {
    Android: function () {
        return navigator.userAgent.match(/Android/i);
    },
    BlackBerry: function () {
        return navigator.userAgent.match(/BlackBerry/i);
    },
    iOS: function () {
        return navigator.userAgent.match(/iPhone|iPad|iPod/i);
    },
    Opera: function () {
        return navigator.userAgent.match(/Opera Mini/i);
    },
    Windows: function () {
        return navigator.userAgent.match(/IEMobile/i);
    },
    any: function () {
        return (isMobile.Android() || isMobile.BlackBerry() || isMobile.iOS() || isMobile.Opera() || isMobile.Windows());
    }
};
let Source;
if (isMobile.any()) {
    Source = DragDropContext(TouchBackend)(dragTest);
}
else {
    Source = DragDropContext(HTML5Backend)(dragTest);
}
export default Source;
