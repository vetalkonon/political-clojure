import React, { PropTypes, Component } from 'react';
import classnames from 'classnames';
import IssuesList from '../step0components/issuesList';

class createIssueContainer extends Component {

    render() {
        return (
            <div className="main-card-container container z-depth-3">
                <IssuesList issues={this.props.issues}
                            selectIssue={this.props.actions.selectIssue}
                            pickRandom={this.props.actions.pickRandom}/>
            </div>
        )
    }
}

createIssueContainer.propTypes = {
    issues: PropTypes.array.isRequired,
    actions: PropTypes.object.isRequired
};

export default createIssueContainer
