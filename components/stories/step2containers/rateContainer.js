import React, { PropTypes, Component } from 'react';
import classnames from 'classnames';
import Slider from 'rc-slider';
import ChooseArgStrength from '../step2components/argumentRadioGroup';
import _ from 'lodash';

class rateContainer extends Component {

    render() {
        return (
            <div className="main-card-container container z-depth-3">
                <span>Rate each of the arguments below relating to this proposal:</span>
                <div className="row">
                    <ChooseArgStrength dropdownActive={"active"}
                                       arguments={this.props.arguments}
                                       choosePos={this.props.actions.choosePos}
                                       supportOrNot={this.props.actions.supportOrNot}
                    />
                </div>
            </div>
        )
    }
}

rateContainer.propTypes = {
    arguments: PropTypes.array.isRequired,
    actions: PropTypes.object.isRequired
};

export default rateContainer
