import React from 'react';
import Header from './global_components/header';
import { storiesOf, action } from '@kadira/storybook';
import { Issues, OneArg, SeveralArgs, ManyArgs, TwoArgs } from './global_components/issues_export';
import RateContainer from './step2containers/rateContainer';
import DragTest from './step6container/dragTest';

storiesOf('Step 2', module)
    .add('Rate individual arguments', () => {
    const state = {
        arguments: OneArg,
        actions: {
            choosePos: action("Choose your position strength"),
            supportOrNot: action("Support of Opposite argument")
        }
    };
        return (
        <div className="main">
            <Header headName="Topic: Developed countries should accept more refugees from crisis regions (e.g, Syrian war)"/>
            <RateContainer arguments={state.arguments} actions={state.actions} />
        </div>
    )})
    .add('Rate individual arguments. Many args', () => {
        const state = {
            arguments: TwoArgs,
            actions: {
                choosePos: action("Choose your position strength"),
                supportOrNot: action("Support of Opposite argument")
            }
        };
        return (
            <div className="main">
                <Header headName="Topic: Developed countries should accept more refugees from crisis regions (e.g, Syrian war)"/>
                <RateContainer arguments={state.arguments} actions={state.actions} />
            </div>
        )});