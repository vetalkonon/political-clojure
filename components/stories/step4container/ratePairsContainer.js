import React, { PropTypes, Component } from 'react';
import classnames from 'classnames';
import Slider from 'rc-slider';
import _ from 'lodash';

class ratePairsContainer extends Component {

    constructor(props){
        super(props);
        this.state = {
            singleSupported: _.sample(props.supported),
            singleOpposed: _.sample(props.opposed)
        }
    }

    makeChoice(value) {
        this.props.actions.choosePos(value)
    }

    render() {
        return (
            <div className="main-card-container container z-depth-3">
                <div className="row">
                    <div className="col s6 m6 l6">
                        <span>Argument (supporting):</span>
                        <div className="argument">
                            {this.state.singleSupported.text}
                        </div>
                    </div>
                    <div className="col s6 m6 l6">
                        <span>Argument (opposing):</span>
                        <div className="argument">
                            {this.state.singleOpposed.text}
                        </div>
                    </div>
                </div>
                <div className="row des makeChoiceBox">
                    <span className="or-divider">Drag the slider to indicate <b>which side</b> of this argument pair is better: </span>
                    <div className="row label-row">
                        <div className="col s12 radio-group">
                            <span>Much better</span>
                        </div>
                        <div className="col s12 radio-group">
                            <span>Better</span>
                        </div>
                        <div className="col s12 radio-group">
                            <span>Neutral</span>
                        </div>
                        <div className="col s12 radio-group">
                            <span>Better</span>
                        </div>
                        <div className="col s12 radio-group">
                            <span>Much better</span>
                        </div>
                    </div>
                    <div className="container">
                        <Slider onChange={this.makeChoice.bind(this)}
                                className="strength-slider"
                                tipFormatter={null}
                                dots
                                min={0}
                                max={4}
                                defaultValue={0}  />
                    </div>
                </div>
            </div>
        )
    }
}

ratePairsContainer.propTypes = {
    actions: PropTypes.object.isRequired,
    supported: PropTypes.array.isRequired,
    opposed: PropTypes.array.isRequired
};

export default ratePairsContainer
