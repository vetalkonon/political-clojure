import React, { PropTypes, Component } from 'react';
import classnames from 'classnames';
import { linkTo } from '@kadira/storybook';
import Slider from 'rc-slider';

class ChooseArgStrength  extends Component {

    constructor(props) {
        super(props);
        this.state = {
            dropdown: false
        }
    }

    supportOrNot(id, e) {
        this.props.supportOrNot(id, e.target.value)
    }

    makeChoice(id, e) {
        if(id === 0) {
            this.setState({
                dropdown: true
            });
        }
        else {
            this.setState({
                dropdown: false
            });
        }
        this.props.choosePos(id);
    }

    renderArguments() {
        let dropClass = classnames({
            "hidden": this.state.dropdown === false
        });
        return this.props.arguments.map((argument) =>  (
            <div className="row des makeChoiceBox" key={argument.id}>
                <div className="col s12 m6 l6 argument-container">
                    <div className="collection argument-box">
                        <div className="collection-item">{argument.text}</div>
                    </div>
                    <div className="input-field supp-op-box">
                        <select onChange={this.supportOrNot.bind(this, argument.id)}>
                            <option value="" disabled selected>Select:</option>
                            <option value="Supporting" defaultValue>Supporting</option>
                            <option value="Against">Against</option>
                        </select>
                    </div>
                </div>
                <div className="col s12 m6 l6" onChange={this.makeChoice.bind(this, argument.id)}>
                    <div className="row label-row">
                        <div className="col s12 radio-group">
                            <span>Not valid</span>
                        </div>
                        <div className="col s12 radio-group">
                            <span>Oppose</span>
                        </div>
                        <div className="col s12 radio-group">
                            <span>Neutral</span>
                        </div>
                        <div className="col s12 radio-group">
                            <span>Support</span>
                        </div>
                        <div className="col s12 radio-group">
                            <span>Strongly support</span>
                        </div>
                    </div>
                    <div className="container">
                        <Slider onChange={this.makeChoice.bind(this)}
                                className="strength-slider"
                                tipFormatter={null}
                                dots
                                min={0}
                                max={4}
                                defaultValue={argument.supStrength}  />
                        <ul id='dropdown2' className={`dropdown-list white z-depth-3 ${dropClass}`}>
                            <li><input type="text" placeholder="Why the argument isn’t valid"/></li>
                        </ul>
                    </div>
                </div>
            </div>
        ));
    }

    render() {
        return (
            <div className="row no-margins">
                {this.renderArguments()}
            </div>
        )
    }
}

ChooseArgStrength.propTypes = {
    choosePos: PropTypes.func.isRequired,
    supportOrNot: PropTypes.func.isRequired,
    arguments: PropTypes.array.isRequired,
    dropdownActive: PropTypes.string.isRequired
};

export default ChooseArgStrength
