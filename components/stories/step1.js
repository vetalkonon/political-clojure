import React from 'react';
import Header from './global_components/header';
import { storiesOf, action } from '@kadira/storybook';
import SupportStrengthContainer from './step1container/supportStrength';

storiesOf('Step 1', module)
    .add('REGISTER initial viewpoint', () => {
        const state = {
            header: "Topic: Developed countries should accept more refugees from crisis regions (e.g, Syrian war)",
            actions: {
                choosePos: action("Choose Position")
            }
        };
        return (
            <div className="main">
                <Header headName={state.header}/>
                <SupportStrengthContainer actions={state.actions}/>
            </div>
        )
    });