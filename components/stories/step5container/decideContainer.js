import React, { PropTypes, Component } from 'react';
import classnames from 'classnames';
import ChoosePosition from '../step1components/choosePosition';

class decideContainer extends Component {

    render() {
        return (
            <div className="main-card-container container z-depth-3">
                <span className="or-divider">After thinking about this in detail, would you say you support or oppose this proposition?</span>
                <ChoosePosition choosePos={this.props.actions.choosePos}/>
            </div>
        )
    }
}

decideContainer.propTypes = {
    actions: PropTypes.object.isRequired
};

export default decideContainer
