import React, { PropTypes, Component } from 'react';
import { DragSource } from 'react-dnd';
import { ItemTypes } from '../global_components/ItemTypes'

var bool = true;
const boxSource = {
    beginDrag(props, monitor) {
        const card = monitor.getItem();
        return {
            id: props.card.id,
            content: props.card.content,
            index: props.index
        };
    },
    endDrag(props, monitor) {
        const item = monitor.getItem();
        const dropResult = monitor.getDropResult();
        item.dragged = true;
        if (dropResult) {
            props.removeFromList(props.card)
            //console.log("WE DROPPED: ", item);
        }
    }
};

function collect(connect, monitor) {
    return {
        connectDragSource: connect.dragSource(),
        isDragging: monitor.isDragging()
    }
}

class DropItem extends React.Component {

    constructor(props) {
        super(props);
        this.state={
            canDrag:true
        };
    }

    render() {
        const { card, connectDragSource } = this.props;

        return connectDragSource(
            <div className="collection">
                <div className="collection-item inline">{card.content}</div>
            </div>
        )}

}

DropItem.propTypes = {
    connectDragSource: PropTypes.func.isRequired,
    removeFromList: PropTypes.func.isRequired,
    isDragging: PropTypes.bool.isRequired,
    card: PropTypes.object.isRequired,
    index: PropTypes.number.isRequired
};
export default DragSource(ItemTypes.CARD, boxSource, collect)(DropItem);
