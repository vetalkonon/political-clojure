import React, { PropTypes, Component } from 'react';
import  update from 'react-addons-update';
import { DropTarget } from 'react-dnd';
import { ItemTypes } from '../global_components/ItemTypes';
import  Card  from './sortableDragCards';
import FaTrash from 'react-icons/lib/fa/trash';
import _ from 'lodash';

const dustbinTarget = {
    drop(props,monitor, component) {
        const item = monitor.getItem();
        if(_.isEmpty(component.state.cards)) {
            component.setState(update(component.state, {
                cards: {
                    $push: [item]
                }
            }))
        }
        else if(!_.includes(component.state.cards, item)){
            component.setState(update(component.state, {
                cards: {
                    $push: [item]
                }
            }))
        }
        props.rankArgsToZero(component.state.cards);
    }
};

function collect(connect, monitor) {
    return {
        connectDropTarget: connect.dropTarget(),
        isOver: monitor.isOver(),
        canDrop: monitor.canDrop()
    }
}

class removeZone extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            cards: []
        };
        this.moveCard = this.moveCard.bind(this);
    }

    moveCard(dragIndex, hoverIndex) {
        const { cards } = this.state;
        const dragCard = cards[dragIndex]; //IS NEEDED FOR REDUCER
        this.setState(update(this.state, {
            cards: {
                $splice: [
                    [dragIndex, 1],
                    [hoverIndex, 0, dragCard]
                ]
            }
        }));
        this.props.rankArgs(this.state.cards);
    }
    renderRankedItems(){
        return _.map(this.state.cards, (card, i) =>{
            return (
                <Card key={i}
                      index={i}
                      card={card}
                      moveCard={this.moveCard} />
            )
        })
    }

    render() {
        const { isOver, canDrop, connectDropTarget } = this.props;
        const isActive = isOver && canDrop;

        let backgroundColor = '#d32f2f';
        if (isActive) {
            backgroundColor = '#f44336';
        } else if (canDrop) {
            backgroundColor = '#b71c1c';
        }

        return connectDropTarget(
            <div className="white-text delete-box" style={{backgroundColor }}>
                <span className="hide-on-med-and-down">Drag bad or irrelevant arguments here: <FaTrash className="margin-left-3" /></span>
                <div className="show-on-med-and-down hide-on-large-only custom-trash"><FaTrash /></div>
                <ul className="collection no-border">
                    {this.renderRankedItems()}
                </ul>
            </div>
        );

    }
}

removeZone.propTypes = {
    connectDropTarget: PropTypes.func.isRequired,
    rankArgsToZero: PropTypes.func.isRequired,
    isOver: PropTypes.bool.isRequired,
    canDrop: PropTypes.bool.isRequired
};

export default DropTarget(ItemTypes.CARD, dustbinTarget, collect)(removeZone);