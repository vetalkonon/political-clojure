import React, { PropTypes, Component } from 'react';
import  update from 'react-addons-update';
import { DropTarget } from 'react-dnd';
import { ItemTypes } from '../global_components/ItemTypes';
import  Card  from './sortableDragCards';
import _ from 'lodash';

const dustbinTarget = {
    drop(props,monitor, component) {
        const item = monitor.getItem();
        if(_.isEmpty(component.state.cards)) {
            component.setState(update(component.state, {
                cards: {
                    $push: [item]
                }
            }))
        }
        else if(!_.includes(component.state.cards, item)){
            component.setState(update(component.state, {
                cards: {
                    $push: [item]
                }
            }))
        }
        props.rankArgs(component.state.cards);
    }
};

function collect(connect, monitor) {
    return {
        connectDropTarget: connect.dropTarget(),
        isOver: monitor.isOver(),
        canDrop: monitor.canDrop()
    }
}

class DropZone extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            cards: []
        };
        this.moveCard = this.moveCard.bind(this);
    }

    moveCard(dragIndex, hoverIndex) {
        const { cards } = this.state;
        const dragCard = cards[dragIndex]; //IS NEEDED FOR REDUCER
        this.setState(update(this.state, {
            cards: {
                $splice: [
                    [dragIndex, 1],
                    [hoverIndex, 0, dragCard]
                ]
            }
        }));
        this.props.rankArgs(this.state.cards);
    }
    renderRankedItems(){
        return _.map(this.state.cards, (card, i) => {
            return (
                <Card key={i}
                      index={i}
                      card={card}
                      moveCard={this.moveCard.bind(this)}
                />
            )
        })
    }

    render() {
        const { isOver, canDrop, connectDropTarget } = this.props;
        const isActive = isOver && canDrop;

        let backgroundColor = '#455a64';
        if (isActive) {
            backgroundColor = '#689f38';
        } else if (canDrop) {
            backgroundColor = 'darkkhaki';
        }

        return connectDropTarget(
            <div style={{backgroundColor }} className="col s12 m12 l12 dropZone">
                <ul className="collection">
                    {this.renderRankedItems()}
                </ul>
            </div>
        );

    }
}

DropZone.propTypes = {
    connectDropTarget: PropTypes.func.isRequired,
    rankArgs: PropTypes.func.isRequired,
    isOver: PropTypes.bool.isRequired,
    canDrop: PropTypes.bool.isRequired
};

export default DropTarget(ItemTypes.CARD, dustbinTarget, collect)(DropZone);
