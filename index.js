// @flow
import 'babel-polyfill';
import React from 'react';
import { render } from 'react-dom';
import { Provider } from 'react-redux';
import App from './containers/App';
import configureStore from './store/configureStore';
import 'todomvc-app-css/index.css';
import { Router, browserHistory } from 'react-router';
import routes from './routes';
import './containers/styles/materialize.min.css';
import './containers/styles/styles.css';
import 'rc-slider/assets/index.css';
import injectTapEventPlugin from 'react-tap-event-plugin';
import _ from 'lodash';

require('./utils/immutable.js');


const store = configureStore();
const ComponentEl = (
        <Router history={browserHistory} routes={routes} />
);
render(
  <Provider store={store}>
      {ComponentEl}
  </Provider>,
  document.getElementById('root')
)
