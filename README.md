## Storybook

### Usage

```
npm install
npm run storybook
```

Then open `http://localhost:9001` on your browser. For more information visit [React Storybook](https://github.com/kadirahq/react-storybook) repo.

## Client + Server

Server written using automatic `GraphQL` schema generation
